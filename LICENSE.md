# License

See the
[guidelines for contributions](https://github.com/git@gitlab.com:chrysn/resource-directory-extensions/blob/master/CONTRIBUTING.md).
