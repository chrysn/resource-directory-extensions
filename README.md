# CoRE Resource Directory Extensions

This is the working area for the individual Internet-Draft, "CoRE Resource Directory Extensions".

* [Datatracker Page](https://datatracker.ietf.org/doc/draft-amsuess-core-resource-directory-extensions)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-amsuess-core-resource-directory-extensions)


## Contributing

See the
[guidelines for contributions](https://gitlab.com/chrysn/resource-directory-extensions/blob/master/CONTRIBUTING.md).

Contributions can be made by creating pull requests.
The GitLab interface supports creating pull requests using the Edit (✏) button.


## Command Line Usage

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

Command line usage requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).

